#ifndef SPARROW_H
#define SPARROW_H

#include <stdint.h>

/* SYSTEM CONFIGURATION ******************************************************/
/* Change the following lines to fit the system on which SPARROW is deployed */

/* Architecture format (Operand order in asm) */
#define asm_r3(op, src1, src2, dest) asm (op " %0, %1, %2":"=r"(dest):"r"(src1),"r"(src2))
#define asm_r2(op, src1, dest) asm (op " %0, %1":"=r"(dest):"r"(src1))

/* Data type (64 or 32 bits) */
typedef uint64_t uword_t;
typedef  int64_t  word_t;
typedef uint64_t uint8x8_t;
typedef  int64_t  int8x8_t;

#define VTYPE_U  uint8x8_t
#define VTYPE_S   int8x8_t

/*****************************************************************************/

/* This library is always optimized as O3 */
#pragma GCC push_options
#pragma GCC optimize ("O3")

/* All function calls will be inlined always */
#define fattr __attribute__((always_inline)) static inline

/* Utils */
#define __ISUNSIGNED(a) (a>=0 && ~a>=0)
#define __ISCONSTANT(a) is_const<int >::a

/* SPARROW macros */
//perform C = op2(A op1 B)
#define __sparrow(op1, op2, src1, src2, dest) \
    asm_r3(op1 "_" op2, src1, src2, dest)

//Perform reduction operation
#define __stage2(dest, op, src) \
        if(__ISUNSIGNED(dest)) asm_r2("unop_" op, src, dest); \
        else asm_r2("nop_" op, src, dest)

/*****************************************************************************/

/* First Stage */
/* ADD */
fattr VTYPE_S vadd_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("add_", src1, src2, dest);
    return dest;
}
fattr VTYPE_S vaddq_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("add_sat", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vadd_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("uadd_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vaddq_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("uadd_sat", src1, src2, dest);
    return dest;
}

/* SUB */
fattr VTYPE_S vsub_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("sub_", src1, src2, dest);
    return dest;
}
fattr VTYPE_S vsubq_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("sub_sat", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vsub_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("usub_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vsubq_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("usub_sat", src1, src2, dest);
    return dest;
}

/* MUL Signed/Unsigned/Mixed */
fattr VTYPE_S vmul_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("mul_", src1, src2, dest);
    return dest;
}
fattr VTYPE_S vmulq_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("mul_sat", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vmul_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("umul_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vmulq_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("umul_sat", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vmul_m8(VTYPE_U src1, VTYPE_S src2){
    VTYPE_U dest;
    asm_r3("mmul_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vmulq_m8(VTYPE_U src1, VTYPE_S src2){
    VTYPE_U dest;
    asm_r3("mmul_sat", src1, src2, dest);
    return dest;
}

/* SHIFT */

/* MAX */
fattr VTYPE_S vmax_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("max_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vmax_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("umax_", src1, src2, dest);
    return dest;
}

/* MIN */
fattr VTYPE_S vmin_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("min_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vmin_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("umin_", src1, src2, dest);
    return dest;
}

/* MERGE (Pass B) */

/* AND / NAND */
fattr VTYPE_S vand_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("and_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vand_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("and_", src1, src2, dest);
    return dest;
}
fattr VTYPE_S vnand_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("nand_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vnand_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("nand_", src1, src2, dest);
    return dest;
}

/* OR / NOR */
fattr VTYPE_S vor_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("or_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vor_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("or_", src1, src2, dest);
    return dest;
}
fattr VTYPE_S vnor_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("nor_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vnor_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("nor_", src1, src2, dest);
    return dest;
}

/* XOR / XNOR */
fattr VTYPE_S vxor_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("xor_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vxor_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("xor_", src1, src2, dest);
    return dest;
}
fattr VTYPE_S vxnor_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("xnor_", src1, src2, dest);
    return dest;
}
fattr VTYPE_U vxnor_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("xnor_", src1, src2, dest);
    return dest;
}

/* Second Stage */

/* SUM */
fattr word_t  vsum_s8(VTYPE_S src){
    VTYPE_S dest;
    asm_r2("nop_sum", src, dest);
    return dest;
}
fattr word_t  vsumq_s8(VTYPE_S src){
    VTYPE_S dest;
    asm_r2("nop_ssum", src, dest);
    return dest;
}
fattr uword_t vsum_u8(VTYPE_U src){
    VTYPE_U dest;
    asm_r2("unop_sum", src, dest);
    return dest;
}
fattr uword_t vsumq_u8(VTYPE_U src){
    VTYPE_U dest;
    asm_r2("unop_ssum", src, dest);
    return dest;
}

/* MAX */
fattr word_t  vlmax_s8(VTYPE_S src){
    VTYPE_S dest;
    asm_r2("nop_max", src, dest);
    return dest;
}
fattr uword_t vlmax_u8(VTYPE_U src){
    VTYPE_U dest;
    asm_r2("unop_max", src, dest);
    return dest;
}

/* MIN */
fattr word_t  vlmin_s8(VTYPE_S src){
    VTYPE_S dest;
    asm_r2("nop_min", src, dest);
    return dest;
}
fattr uword_t vlmin_u8(VTYPE_U src){
    VTYPE_U dest;
    asm_r2("unop_min", src, dest);
    return dest;
}

/*****************************************************************************/
/* Aliases and utils */
/* DOT product */
fattr word_t  dot_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("dot", src1, src2, dest);
    return dest;
}
fattr word_t  dotq_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("sdot", src1, src2, dest);
    return dest;
}
fattr uword_t dot_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("udot", src1, src2, dest);
    return dest;
}
fattr uword_t dotq_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("usdot", src1, src2, dest);
    return dest;
}
fattr uword_t dot_m8(VTYPE_U src1, VTYPE_S src2){
    VTYPE_U dest;
    asm_r3("mdot", src1, src2, dest);
    return dest;
}
fattr uword_t dotq_m8(VTYPE_U src1, VTYPE_S src2){
    VTYPE_U dest;
    asm_r3("msdot", src1, src2, dest);
    return dest;
}

/* MAX of MAX*/
fattr word_t  max_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("max_max", src1, src2, dest);
    return dest;
}
fattr uword_t max_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("umax_max", src1, src2, dest);
    return dest;
}

/* MIN of MIN*/
fattr word_t  min_s8(VTYPE_S src1, VTYPE_S src2){
    VTYPE_S dest;
    asm_r3("min_min", src1, src2, dest);
    return dest;
}
fattr uword_t min_u8(VTYPE_U src1, VTYPE_U src2){
    VTYPE_U dest;
    asm_r3("umin_min", src1, src2, dest);
    return dest;
}
/*****************************************************************************/

/* LOAD data */
fattr uword_t vget_1d(uint8_t * array, uint32_t offset){
    return *((uword_t*) &array[offset]);
}

fattr uword_t vget_2d(uint8_t * array[], uint32_t i, uint32_t j){
    return *((uword_t*) &array[i][j]);
}

#undef fattr
#pragma GCC pop_options
#endif //SPARROW_H

